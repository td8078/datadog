package com.mfi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class DatadogApplication {

	
	private static final Logger log = LoggerFactory.getLogger(DatadogApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DatadogApplication.class, args);
	}

	@Component	
	public class AppRunner implements ApplicationRunner {

		@Override
		public void run(ApplicationArguments args) throws Exception {
			log.info("hello world");
			
		}
		
	}
}
